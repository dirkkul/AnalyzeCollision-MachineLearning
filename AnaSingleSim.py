# Copyright Dirk Kulawiak. This scripts can be used to analyze cell-cell collisions done with https://gitlab.com/dirkkul/Phasefield-GPU
# More informations about this scripts can be found at https://gitlab.com/dirkkul/AnalyzeCollision-MachineLearning

import pickle
import numpy as np
import pandas as pd
import os
import math
import sys
import csv
import inspect

from configparser import SafeConfigParser
import argparse

def get_script_dir(follow_symlinks=True):
	"""Get the current location of this script. This is important to load other files that are located in the folder."""
	if getattr(sys, 'frozen', False):
		path = os.path.abspath(sys.executable)
	else:
		path = inspect.getabsfile(get_script_dir)
	if follow_symlinks:
		path = os.path.realpath(path)
	return os.path.dirname(path)

def walklevel(some_dir, level=1):
	some_dir = some_dir.rstrip(os.path.sep)
	assert os.path.isdir(some_dir)
	num_sep = some_dir.count(os.path.sep)
	for root, dirs, files in os.walk(some_dir):
		yield root, dirs, files
		num_sep_this = root.count(os.path.sep)
		if num_sep + level <= num_sep_this:
			del dirs[:]
			
def signum(x):
	return (x > 0) - (x < 0)

def CellsInContact(Pos, Radius):
	"""Check if the cells have been in contact (their distance is less than X) and return a bool."""
	Dist = np.abs(Pos[0,:,1] - Pos[1,:,1])
	if np.min(Dist) < 1.5*Radius:
		return True
	return False

def CellTurn(Position, CellNumber):
	"""Detect if the cells have turned around return the number of cells that turned (0 = no cell turned, 1 = one cell turned, 2 = both) """
	CellTurn = np.zeros(CellNumber)
	for Cell in range(CellNumber):
		sign = signum(Position[Cell][0][1] - Position[Cell][1][1])
		
		for i in range(1,len(Position[Cell])):
			if signum(Position[Cell][i][1] - Position[Cell][i][1]) != sign:
				sign = signum(Position[Cell][i][1] - Position[Cell][i][1])
				CellTurn[Cell] = 1
	return CellTurn.sum()

def CellPass(Position, CellNumber):
	"""Detect if the cells have passed each other."""
	signStart = np.sign(Position[0][0][1] - Position[1][0][1])
	signEnd = np.sign(Position[0][-1][1] - Position[1][-1][1])
	if signStart == signEnd:
		return 0
	return 1


def AnalyseFolder(fold):
	"""Analyze a collision between cells. Gather (simulation) parameters and information about the collision outcome
		to train a ML model and/or predict the outcome of these collisions. Write everything to a pandas df that can be loaded
		by the training script.
		Write this dataframe to disk and return it to be able to collect all results.
	"""
	
	df = pd.DataFrame(index=[fold],columns=columns+Parameter)
	
	# read predictions from (old) analysis script. This data can be used to train a model.
	if os.path.exists(os.path.join(fold,'Data','CollsionOutcome.dat')):
		with open(os.path.join(fold,'Data','CollsionOutcome.dat')) as f:
			df.loc[fold]['Result'] = int(f.readline().split('\t')[0].strip())
			
	
	#The old parameter files have non-optimal names. This is compatible with old and new ones
	parser = SafeConfigParser()
	try:
		parser.read(os.path.join(fold,'params.ini'))
		CellNumber = parser.getint('main','CellNumber')
		df.loc[fold]['k_cr'] = parser.getfloat('main','k_cr')
		df.loc[fold]['k_fr'] = -0. #not there (yet)
	except:
		parser.read(os.path.join(fold,'params.dat'))
		CellNumber = parser.getint('main','numcell')
		df.loc[fold]['k_cr'] = parser.getfloat('main','k_I')
		df.loc[fold]['k_fr'] = parser.getfloat('main','k_Iff')
	
	SaveTime = parser.getfloat('main','SaveTime')
	Radius = parser.getfloat('main','cellradius')
	
	Position = [[] for i in range(CellNumber)]
	TimePoints = []
	with open(os.path.join(fold,'positions.dat')) as f:
		reader = csv.reader(f, delimiter='\t')
		for row in reader:
			TimePoints.append(float(row[0]))
			for Cell in range(CellNumber):
				PosX = float(row[1 + Cell * 2])
				PosY = float(row[2 + Cell * 2])
				Position[Cell].append( [PosX, PosY] )
	Position = np.array(Position)
	
	Vel = []
	for Cell in range(CellNumber):
		Vel.append([np.diff(Position[Cell,:,0])/SaveTime, np.diff(Position[Cell,:,1])/SaveTime])
	Vel = np.array(Vel)
	
	Direction = [[] for i in range(CellNumber)]
	for Cell in range(CellNumber):
		for i in range(len(Vel[Cell][0])):
			Direction[Cell].append(math.atan2(Vel[Cell][1][i], Vel[Cell][0][i]) )
	
	#Rotational Order Parameter
	OrderParamDirVel = []
	SumDirVelX = 0
	SumDirVelY = 0
	for Cell in range(CellNumber):
		for i in range(len(Direction)):
			Angle = (Direction[Cell][i] + math.pi )/ (2.0 * math.pi) * 360.0 
			SumDirVelX +=  math.cos( Angle ) * Vel[0,0,i]
			SumDirVelY +=  math.sqrt( 1 - math.cos( Angle )**2 ) * Vel[0,1,i]
	OrderParamDirVel.append(1.0/CellNumber  * (SumDirVelX + SumDirVelY)  )
	
	#Compute features, we can use to predict the collision type
	df.loc[fold]['Distance'] = np.abs(Position[0,-1,1] - Position[1, -1, 1])
	df.loc[fold]['Contact'] = CellsInContact(Position, Radius)
	
	df.loc[fold]['Pass'] = CellPass(Position, CellNumber)
	df.loc[fold]['Turn'] = CellTurn(Position, CellNumber)
	df.loc[fold]['MeanVelEnd'] =  np.mean(np.abs([Vel[0,0,-1], Vel[1,0,-1], Vel[0,1,-1], Vel[1,1,-1]]))
	df.loc[fold]['RotOrder'] =  np.mean(OrderParamDirVel[len(OrderParamDirVel)//2:])
	
	#Predict the outcome with a trained model
	if os.path.exists(os.path.join(workdir,'Best_model.pkl')) and Predict:
		
		cfl = pd.read_pickle(os.path.join(workdir,'Best_model.pkl'))
		predData = cfl[2].transform(df[cfl[1]])
		df.loc[fold]['ResultPrediction'] = cfl[0].predict(predData)[0]
	
	pickle.dump(df, open(os.path.join(fold,'df.pkl'), 'wb'), -1)
	return df

#commandline arguments
parser = argparse.ArgumentParser(description='Analyse Cell-Cell Collisions')
parser.add_argument("inputDir", help="input Directory")
parser.add_argument('-r', action='store_true', default=False, help="Redo all folders or only do new ones")
parser.add_argument('-p', action='store_true', default=False, help="Predict outcome")
parser.add_argument('-s', action='store_true', default=True, help="Combine all data to single df")

args = parser.parse_args()
path = args.inputDir
Predict = args.p
Redo = args.r
CombineData = args.s

workdir = get_script_dir()

#empty Dataframe
Parameter = ['k_cr', 'k_fr']
columns=['Result','ResultPrediction','Distance','Contact','Turn', 'Pass', 'MeanVelEnd','RotOrder']

if CombineData:
	dfCombine = pd.DataFrame( columns=columns+Parameter)

#walk through the input folder and check for simulation folders. Analyze each of them independently.
Folder = []
for fold in walklevel(path,level=3):
	if not os.path.exists(os.path.join(fold[0],'positions.dat')):
		continue
	if os.path.exists(os.path.join(fold[0],'CRASH')):
		continue
	if os.path.exists(os.path.join(fold[0],'df.pkl')) and not Redo:
		continue
	Folder.append(fold[0])

for f in Folder:
	dfreturn = AnalyseFolder(f)
	if CombineData:
		dfCombine = dfCombine.append(dfreturn)

if CombineData:
	pickle.dump(dfCombine, open(os.path.join(path,'df_sum.pkl'), 'wb'), -1)
