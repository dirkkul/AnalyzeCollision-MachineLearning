# Introduction

This repository contains scripts to analyze the output of [my CUDA code to model cell-cell collisions](https://gitlab.com/dirkkul/Phasefield-GPU).

These scripts can:

-   Collect data from collisions including manual classification (if available)
-   Train a model with the collected data*
-   Predict the outcome of simulations

*classified collision data itself is not available due to their large size. Summarized data to train the model is available in the `TrainingData` folder.

The model is currently using the `scikit-learn` module.

# Usage
The scripts are written with python3, although it should be trivial to make them compatible with python2.

## Analyzing and Prediction
You can apply `AnaSingleSim.py` to a folder with simulations using `python3 AnaSingleSim.py [Options] *folder*`.
A folder can contain a single simmulation or (several) subfolders with more simulations.
The script will automatically walk through everything (for deep trees you have to change the level paramter).
Results will be written (with pickle) to `df.pkl` in each simulation folder
The predictions are based on a model with the name `Best_model.pkl` which can be created with the training script.

Options for `AnaSingleSim.py` can be combined and include:
-   -r to analyze simulations again [Default: ignore everything that has been analyzed before].
-   -s to combine all results to a single pandas dataframe [Default: Off]
-   -p to predict collision outcome based on a provided model [Default: Off]

__Please note:__ If your simulations are too long and the cells collide a second time (through the periodic boundaries) the result will be _wrong_.

## Training
You can train a model yourself with the provided data. 
Simply open the provided training script with jupyter, execute it and it will output a model which can be used with `AnaSingleSim.py`.
Feel free to experiment with the model or to create your own training data!


# Todo
-   Plot a few results: Position/Velocity/Direction over time
-   Create a tensorflow module